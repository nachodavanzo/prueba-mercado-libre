﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using MercadoPago.Client.Common;
using MercadoPago.Client.Payment;
using MercadoPago.Config;
using MercadoPago.Resource.Payment;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.Globalization;
using PruebaMercadoLibre.Shared;

namespace PruebaMercadoLibre.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MercadoPagoController : ControllerBase
    {
        public MercadoPagoController() {}

        [HttpPost]
        public async Task MercadoPago()
        {
            MercadoPagoConfig.AccessToken = "TEST-7393201922405572-041012-86b78013163dc627a2c063ce1e31ac31-420974459";
            try
            {
                using (var reader = new StreamReader(HttpContext.Request.Body))
                {
                    var body = reader.ReadToEndAsync();


                    JsonSerializerSettings settings = new JsonSerializerSettings
                    {
                        DateFormatString = "yyyy-MM-ddTHH:mm:ss",
                        //MissingMemberHandling = MissingMemberHandling.Error,
                        Culture = new CultureInfo("es-ES"),
                        NullValueHandling = NullValueHandling.Ignore,
                        Formatting = Formatting.Indented
                    };

                    var paymentToSend = JsonConvert.DeserializeObject<PaymentRequest>(body.Result, settings);

                    var paymentRequest = new PaymentCreateRequest
                    {
                        TransactionAmount = 100,
                        Token = paymentToSend.token,
                        Description = "Donación realizada",
                        Installments = paymentToSend.installments,
                        PaymentMethodId = paymentToSend.payment_method_id,
                        Payer = new PaymentPayerRequest
                        {
                            Email = paymentToSend.payer.email,
                            Identification = new IdentificationRequest
                            {
                                Type = paymentToSend.payer.identification.type,
                                Number = paymentToSend.payer.identification.number,
                            }
                            //FirstName = Request.Form["cardholderName"]
                        },
                    };

                    var client = new PaymentClient();
                    Payment payment = await client.CreateAsync(paymentRequest);

                }

            }
            catch (Exception ex)
            {
                var message = ex.Message;
                throw;
            }

        }
    }
}
