﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaMercadoLibre.Shared
{
    public class PaymentRequest
    {
        public string token { get; set; } = string.Empty;
        public string issuer_id { get; set; } = string.Empty;
        public string payment_method_id { get; set; } = string.Empty;
        public decimal? transaction_ammount { get; set; }
        public int installments { get; set; }
        public Payer payer { get; set; } = new Payer();
    }

    public class Payer
    {
        public string email { get; set; } = string.Empty;
        public Identification identification { get; set; } = new Identification();
    }

    public class Identification
    {
        public string type { get; set; } = string.Empty;
        public string number { get; set; } = string.Empty;
    }
}
