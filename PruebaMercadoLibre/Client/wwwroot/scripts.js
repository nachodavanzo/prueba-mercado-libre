﻿function initMercadoPago() {
    const mp = new MercadoPago('TEST-58eb5c11-45c8-4cb9-8818-f80774b2e365', {
        locale: 'es-AR'
    });
    const bricksBuilder = mp.bricks();
    const renderCardPaymentBrick = async (bricksBuilder) => {
        const settings = {
            initialization: {
                amount: 100, // monto a ser pago
                payer: {
                    email: "",
                },
            },
            customization: {
                visual: {
                    style: {
                        theme: 'default', // | 'dark' | 'bootstrap' | 'flat'
                    }
                },
            },
            callbacks: {
                onReady: () => {
                    // callback llamado cuando Brick esté listo
                },
                onSubmit: (cardFormData) => {
                    //  callback llamado cuando el usuario haga clic en el botón enviar los datos
                    //  ejemplo de envío de los datos recolectados por el Brick a su servidor
                    return new Promise((resolve, reject) => {
                        fetch("api/mercadopago", {
                            method: "POST",
                            dataType: "json",
                            headers: {
                                "Content-Type": "application/json; charset=utf-8",
                            },
                            body: JSON.stringify(cardFormData)
                        })
                            .then((response) => {
                                // recibir el resultado del pago
                                resolve();
                            })
                            .catch((error) => {
                                // tratar respuesta de error al intentar crear el pago
                                reject();
                            })
                    });
                },
                onError: (error) => {
                    // callback llamado para todos los casos de error de Brick
                },
            },
        };
        window.cardPaymentBrickController = await bricksBuilder.create('cardPayment', 'cardPaymentBrick_container', settings);
    };
    renderCardPaymentBrick(bricksBuilder);

}