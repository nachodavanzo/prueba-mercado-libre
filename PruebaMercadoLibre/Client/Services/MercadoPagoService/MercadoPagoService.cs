﻿using System;
using MercadoPago.Client.Common;
using MercadoPago.Client.Payment;
using MercadoPago.Config;
using MercadoPago.Resource.Payment;

namespace Help2Help.Client.Services.MercadoPagoService
{
    public class MercadoPagoService : IMercadoPagoService
    {
        private readonly HttpClient http;

        public event Action OnChange;
        public MercadoPagoService(HttpClient http)
        {
            this.http = http;
        }

        public async Task MercadoPago()
        {
            //string token = await this.localStorage.GetItemAsync<string>("authToken");
            //var request = new CommentResponseDto() 
            //{ 
            //    PublicationId = publicationId, 
            //    Message = message,
            //    Jwt = token != null ? token : string.Empty 
            //};

            MercadoPagoConfig.AccessToken = "TEST-7393201922405572-041012-86b78013163dc627a2c063ce1e31ac31-420974459";

            //var paymentRequest = new PaymentCreateRequest
            //{
            //    TransactionAmount = decimal.Parse(Request["transactionAmount"]),
            //    Token = Request["token"],
            //    Description = Request["description"],
            //    Installments = int.Parse(Request["installments"]),
            //    PaymentMethodId = Request["paymentMethodId"],
            //    Payer = new PaymentPayerRequest
            //    {
            //        Email = Request["cardholderEmail"],
            //        Identification = new IdentificationRequest
            //        {
            //            Type = Request["identificationType"],
            //            Number = Request["identificationNumber"],
            //        },
            //        FirstName = Request["cardholderName"]
            //    },
            //};

            var paymentRequest = new PaymentCreateRequest
            {
                TransactionAmount = 1,
                Token = "12432",
                Description = "Ignacio Davanzo",
                Installments = 1,
                PaymentMethodId = "Credit",
                Payer = new PaymentPayerRequest
                {
                    Email = "nachodavanzo@gmail.com",
                    Identification = new IdentificationRequest
                    {
                        Type = "Davanzo",
                        Number = "254654856465",
                    },
                    FirstName = "Ignacio"
                },
            };

            var client = new PaymentClient();
            Payment payment = await client.CreateAsync(paymentRequest);

            //var response = await this.http.PostAsJsonAsync("api/comment", request);

            //var publication = await response.Content.ReadFromJsonAsync<ServiceResponse<CommentResponseDto>>();

            //var pub = this.publicationService.PublicationsDTO.FirstOrDefault(x => x.Id == publicationId);
            //pub.Comments.Add(publication?.Data);

            //return pub.Comments;
        }

    }
}
