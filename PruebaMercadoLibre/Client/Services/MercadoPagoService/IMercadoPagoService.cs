﻿namespace Help2Help.Client.Services.MercadoPagoService
{
    public interface IMercadoPagoService
    {
        event Action OnChange;
        Task MercadoPago();
    }
}
